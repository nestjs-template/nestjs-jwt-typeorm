FROM node:21-alpine as builder
WORKDIR /app

COPY package*.json .
RUN npm i

COPY . .
RUN npm run build

FROM node:21-alpine
ENV PORT 4000

USER node

WORKDIR /app

COPY package*.json .

RUN npm i
COPY --from=builder /app/dist ./dist
COPY --from=builder /app/migrations ./migrations

EXPOSE $PORT

CMD ["npm", "run", "start:prod"]
