import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import fastifyHelmet from '@fastify/helmet';
import { FastifyHelmetOptions } from '@fastify/helmet';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import { SwaggerModule } from '@nestjs/swagger';
import { ValidationPipe } from '@nestjs/common';
import { configDocumentBuilder } from '@lib/swagger';

async function bootstrap() {
  const { NODE_ENV, API_HOST } = process.env;
  const PORT = 4000;

  const config = configDocumentBuilder;
  config.addBearerAuth();

  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    new FastifyAdapter(),
  );
  await app.register(fastifyHelmet, {
    contentSecurityPolicy: false,
  } as FastifyHelmetOptions);

  app.useGlobalPipes(new ValidationPipe({ transform: true }));
  const document = SwaggerModule.createDocument(app, config.build());

  SwaggerModule.setup('api/api-docs', app, document);

  app.use('/swagger.json', (req, res) => {
    res.send(document);
  });

  app.setGlobalPrefix('api');
  await app.listen(PORT);
  console.log(`
      Mode: ${NODE_ENV}
      Application is running on ${PORT} port
      OpenAPI docs is available at: ${API_HOST}/api/api-docs
      Swagger.json is available at: ${API_HOST}/swagger.json
  `);
}
bootstrap();
