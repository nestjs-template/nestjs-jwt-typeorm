import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserEntity } from './entities/user.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(UserEntity)
    public userEntityRepository: Repository<UserEntity>,
  ) {}

  findAll() {
    return this.userEntityRepository.find();
  }

  findOne(id: string) {
    return this.userEntityRepository.findOne({
      where: { id },
    });
  }

  findByEmail(email: string) {
    return this.userEntityRepository.findOne({
      where: { email },
    });
  }

  private createEntity(data) {
    const entity = this.userEntityRepository.create();
    return this.userEntityRepository.merge(entity, data);
  }

  create(data: CreateUserDto) {
    const userEntity = this.createEntity(data);
    return this.userEntityRepository.save(userEntity);
  }

  async update(id: string, updateUserDto: UpdateUserDto) {
    return this.userEntityRepository.update(id, updateUserDto);
  }

  remove(id: string) {
    return this.userEntityRepository.delete(id);
  }
}
