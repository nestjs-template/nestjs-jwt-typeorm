import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  Index,
  OneToMany,
} from 'typeorm';
import { OAuthProviderEntity } from '../../auth/entities/oauth-provider.entity';
import { RefreshTokenEntity } from '../../auth/entities/refresh-token.entity';

@Entity('User')
@Index(['id'])
export class UserEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn({ nullable: true })
  updated_at: Date;

  @Column({ nullable: true })
  username: string;

  @Column({ nullable: true })
  password: string;

  @Column({ nullable: true })
  avatar_url: string;

  @Column({ nullable: true })
  email: string;

  @Column({ nullable: true, type: 'timestamp' })
  email_verified: Date;

  @OneToMany(
    () => OAuthProviderEntity,
    (accountProvider) => accountProvider.user,
  )
  account_providers: OAuthProviderEntity[];

  @OneToMany(() => RefreshTokenEntity, (refreshToken) => refreshToken.user)
  refresh_tokens: RefreshTokenEntity[];
}
