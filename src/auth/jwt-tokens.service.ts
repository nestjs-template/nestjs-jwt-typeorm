import { Injectable, UnauthorizedException } from '@nestjs/common';
import { ResponsePayloadDto } from './dto';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { jwt_config } from './config/jwt.config';
import { RefreshTokenEntity } from './entities/refresh-token.entity';
import { JwtPayload, JwtPayloadToken } from './intrefaces';

@Injectable()
export class JwtTokensService {
  public constructor(
    @InjectRepository(RefreshTokenEntity)
    public refreshTokenEntityRepository: Repository<RefreshTokenEntity>,
    private readonly jwtService: JwtService,
  ) {}

  public generateTokens(
    payload: JwtPayload,
    refreshToken?: string,
  ): ResponsePayloadDto {
    const accessTokenCreated = this.createAccessToken(payload);
    const refreshTokenCreated = refreshToken
      ? refreshToken
      : this.createRefreshToken(payload);

    return new ResponsePayloadDto(accessTokenCreated, refreshTokenCreated);
  }

  public createAccessToken(payload: JwtPayload) {
    return this.jwtService.sign(payload, {
      secret: jwt_config.secret,
      expiresIn: jwt_config.expiresIn,
    });
  }
  public createRefreshToken(payload: JwtPayload) {
    return this.jwtService.sign(payload, {
      secret: jwt_config.refresh_secret,
      expiresIn: jwt_config.refresh_expiresIn,
    });
  }

  public validateAccessToken(accessToken: string): JwtPayloadToken {
    try {
      return this.jwtService.verify(accessToken, {
        secret: jwt_config.secret,
      });
    } catch (error) {
      throw new UnauthorizedException('Invalid access token');
    }
  }

  public validateRefreshToken(refreshToken: string): JwtPayloadToken {
    try {
      return this.jwtService.verify(refreshToken, {
        secret: jwt_config.refresh_secret,
      });
    } catch (error) {
      throw new UnauthorizedException('Invalid refresh token');
    }
  }

  public async saveRefreshToken(user_id: string, refresh_token: string) {
    const refreshTokenEntity = this.createEntity({
      user_id: user_id,
      token: refresh_token,
    });

    return this.refreshTokenEntityRepository.save(refreshTokenEntity);
  }

  public async findByToken(refreshToken: string) {
    return this.refreshTokenEntityRepository.findOne({
      where: { token: refreshToken },
    });
  }

  public async removeRefreshToken(refreshToken: string): Promise<void> {
    const { id } = await this.findByToken(refreshToken);
    await this.refreshTokenEntityRepository.delete(id);
  }

  private createEntity(data) {
    const entity = this.refreshTokenEntityRepository.create();
    return this.refreshTokenEntityRepository.merge(entity, data);
  }
}
