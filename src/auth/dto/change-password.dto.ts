import { PickType } from '@nestjs/swagger';
import { SignUpDto } from './sign-up.dto';

export class ChangePasswordDto extends PickType(SignUpDto, [
  'password',
] as const) {
  public password: string;
}
