import { ApiProperty } from '@nestjs/swagger';

export class ResponsePayloadDto {
  @ApiProperty({
    example: '',
  })
  public access_token: string;

  @ApiProperty({
    example: '',
  })
  public refresh_token: string;

  public constructor(accessToken: string, refreshToken: string) {
    this.access_token = accessToken;
    this.refresh_token = refreshToken;
  }
}
