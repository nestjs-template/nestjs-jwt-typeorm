import { ApiProperty } from '@nestjs/swagger';
import { UserEntity } from '../../users/entities/user.entity';
import { ResponsePayloadDto } from './response-payload.dto';

export class AuthorizedUserDto {
  @ApiProperty({ example: '8468b290-fcef-4e11-bf22-b2f99fdc9711' })
  public id: string;

  @ApiProperty({ example: 'user@test.com' })
  public email: string;

  @ApiProperty({ example: 'user@test.com' })
  public username: string;

  @ApiProperty({ example: '2024-02-23T17:52:19.951Z' })
  public email_verified: Date;

  @ApiProperty({ example: '' })
  public avatar_url: string;

  @ApiProperty({
    example: '',
  })
  public access_token: string;

  @ApiProperty({
    example: '',
  })
  public refresh_token: string;

  public constructor(user: UserEntity, tokens: ResponsePayloadDto) {
    this.id = user.id;
    this.email = user.email;
    this.email_verified = user.email_verified;
    this.avatar_url = user.avatar_url;
    this.username = user.username;
    this.access_token = tokens.access_token;
    this.refresh_token = tokens.refresh_token;
  }
}
