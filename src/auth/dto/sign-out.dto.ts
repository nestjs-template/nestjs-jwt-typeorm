import { PickType } from '@nestjs/swagger';
import { ResponsePayloadDto } from './response-payload.dto';

export class SignOutDto extends PickType(ResponsePayloadDto, [
  'refresh_token',
] as const) {
  public refresh_token: string;
}
