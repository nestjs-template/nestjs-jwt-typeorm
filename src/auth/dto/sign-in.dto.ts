import { ApiProperty } from '@nestjs/swagger';
import {
  IsEmail,
  IsNotEmpty,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';

export class SignInDto {
  @ApiProperty({ example: 'user@test.com' })
  @IsNotEmpty()
  @IsEmail()
  public email: string;

  @ApiProperty({ example: '57vQ72rcoCJm' })
  @IsNotEmpty()
  @IsString()
  @MinLength(6)
  @MaxLength(20)
  public password: string;
}
