import { PickType } from '@nestjs/swagger';
import { SignUpDto } from './sign-up.dto';

export class ResetPasswordDto extends PickType(SignUpDto, ['email'] as const) {
  public email: string;
}
