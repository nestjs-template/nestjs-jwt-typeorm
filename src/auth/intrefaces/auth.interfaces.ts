export enum OAuthProviderName {
  google = 'google',
  github = 'github',
  yandex = 'yandex',
}
