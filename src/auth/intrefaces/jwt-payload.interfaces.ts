export interface JwtPayload {
  id: string;
  email: string;
  username: string;
}
export interface JwtPayloadToken extends JwtPayload {
  iat: number;
  exp: number;
}
