import {
  Body,
  Controller,
  HttpCode,
  HttpStatus,
  Patch,
  Post,
  Put,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import {
  AuthorizedUserDto,
  ChangePasswordDto,
  RefreshDto,
  ResponsePayloadDto,
  SignInDto,
  SignOutDto,
  SignUpDto,
} from './dto';
import { ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { Auth, JwtPayloadObject } from './decorator';
import { JwtPayload } from './intrefaces';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @Post('signUp')
  @ApiOkResponse({ type: AuthorizedUserDto })
  async signUp(@Body() body: SignUpDto): Promise<AuthorizedUserDto> {
    return this.authService.signUp(body);
  }

  @Post('signIn')
  @HttpCode(HttpStatus.OK)
  @ApiOkResponse({ type: AuthorizedUserDto })
  async signIn(@Body() body: SignInDto): Promise<AuthorizedUserDto> {
    return this.authService.signIn(body);
  }

  @Auth()
  @Post('signOut')
  @HttpCode(HttpStatus.OK)
  async signOut(@Body() body: SignOutDto) {
    return this.authService.signOut(body);
  }

  @Patch('refresh')
  async refresh(@Body() body: RefreshDto): Promise<ResponsePayloadDto> {
    return this.authService.refresh(body);
  }

  @Auth()
  @Patch('change-password')
  async changePassword(
    @JwtPayloadObject() jwtPayload: JwtPayload,
    @Body() body: ChangePasswordDto,
  ) {
    return this.authService.changePassword(jwtPayload, body);
  }

  /*
  // Отправка на почту
  @Put('reset-password')
  async resetPassword(@Body() body: any) {
    return body;
  }
  // Отправка на почту
  @Put('verify-email')
  async verifyEmail(@Body() body: any) {
    return body;
  }

  // Проверка почты
  @Get('verifed-email')
  async verifedEmail(@Body() body: any) {
    return body;
  }*/
}
