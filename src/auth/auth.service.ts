import {
  Injectable,
  BadRequestException,
  UnauthorizedException, Logger,
} from '@nestjs/common';
import { UsersService } from '../users/users.service';
import {
  ChangePasswordDto,
  ResponsePayloadDto,
  RefreshDto,
  SignInDto,
  SignUpDto,
  SignOutDto,
} from './dto';
import { compare, hash } from 'bcrypt';
import { UserEntity } from '../users/entities/user.entity';
import { AuthorizedUserDto } from './dto';
import { JwtTokensService } from './jwt-tokens.service';
import { JwtPayload } from './intrefaces';

@Injectable()
export class AuthService {
  private readonly logger = new Logger(AuthService.name);
  constructor(
    private usersService: UsersService,
    private jwtTokensService: JwtTokensService,
  ) {}

  async signIn(data: SignInDto) {
    const { email, password } = data;
    const user = await this.usersService.findByEmail(email);
    const valid = user ? await this.validatePassword(password, user) : false;

    if (!valid) {
      throw new UnauthorizedException('The login or password is invalid');
    }

    this.logger.log(`Sign in user ${email}`);
    return this.authorizeUser(user);
  }

  async signUp(data: SignUpDto) {
    const { email, password } = data;
    const user = await this.usersService.findByEmail(email);
    if (user) {
      throw new BadRequestException('Email already used');
    }
    data.password = await this.generatePassword(password);
    const createdUser = await this.usersService.create(data);

    this.logger.log(`Sign up user ${email}`);
    return this.authorizeUser(createdUser);
  }

  async signOut(data: SignOutDto): Promise<void> {
    const { email } = this.jwtTokensService.validateRefreshToken(data.refresh_token);
    this.logger.log(`Sign out user ${email}`);
    await this.jwtTokensService.removeRefreshToken(data.refresh_token);
  }

  async refresh({ refresh_token }: RefreshDto): Promise<ResponsePayloadDto> {
    const { email, id, username } =
      this.jwtTokensService.validateRefreshToken(refresh_token);

    const payloadDto = this.jwtTokensService.generateTokens(
      { email, id, username },
      refresh_token,
    );

    this.logger.log(`User ${email} refreshed to new jwt token`);
    return payloadDto;
  }

  async changePassword(
    payload: JwtPayload,
    { password }: ChangePasswordDto,
  ): Promise<void> {
    const { id: userId, email } = payload;
    const dataUpdate = {
      password: await this.generatePassword(password),
    };
    await this.usersService.update(userId, dataUpdate);
    this.logger.log(`User ${email} changed to new password`);
  }

  private async authorizeUser(user: UserEntity): Promise<AuthorizedUserDto> {
    const { id, email, username } = user;
    const tokensDto = this.jwtTokensService.generateTokens({
      id,
      email,
      username,
    });
    await this.jwtTokensService.saveRefreshToken(id, tokensDto.refresh_token);

    this.logger.log(`Authorized user ${email} `);
    return new AuthorizedUserDto(user, tokensDto);
  }

  private async generatePassword(password: string) {
    return hash(password, 16);
  }

  private async validatePassword(
    password: string,
    user: UserEntity,
  ): Promise<boolean> {
    return compare(password, user.password);
  }
}
