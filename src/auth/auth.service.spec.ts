import { Test, TestingModule } from '@nestjs/testing';
import { AuthService } from './auth.service';
import {
  AuthorizedUserDto,
  ChangePasswordDto,
  RefreshDto,
  ResponsePayloadDto,
  SignInDto,
  SignOutDto,
  SignUpDto,
} from './dto';

import { UserEntity } from '../users/entities/user.entity';
import { JwtTokensService } from './jwt-tokens.service';
import { JwtPayload } from './intrefaces';
import { UsersService } from '../users/users.service';
import { RefreshTokenEntity } from './entities/refresh-token.entity';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { BadRequestException, UnauthorizedException } from '@nestjs/common';

const payloadDto: JwtPayload = {
  id: '8468b290-fcef-4e11-bf22-b2f99fdc9711',
  email: 'user@test.com',
  username: 'User',
};

let userEntity: Partial<UserEntity> = {
  id: '8468b290-fcef-4e11-bf22-b2f99fdc9711',
  email: 'user@test.com',
  password: '$2b$16$6BpvBtD8o5GO2.UNhFRzeO0JcyJiCwGA57LbjnjgEcy2sNxETqSDi',
  email_verified: null,
  avatar_url: null,
  username: 'User',
};

const refreshTokenEntity: Partial<RefreshTokenEntity> = {
  id: '8468b290-fcef-4e11-bf22-b2f99fdc9711',
  token: 'refresh.token',
};

let authorizedUserDto: AuthorizedUserDto = {
  id: '8468b290-fcef-4e11-bf22-b2f99fdc9711',
  email: 'user@test.com',
  email_verified: null,
  avatar_url: null,
  username: 'User',
  access_token: 'access.token',
  refresh_token: 'refresh.token',
};

let responsePayloadDto: ResponsePayloadDto = {
  access_token: 'access.token',
  refresh_token: 'refresh.token',
};

describe('AuthService', () => {
  let authService: AuthService;
  let usersService: UsersService;
  let jwtTokensService: JwtTokensService;
  let refreshTokenRepository: Repository<RefreshTokenEntity>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        AuthService,
        {
          provide: UsersService,
          useValue: {
            findByEmail: jest.fn().mockImplementation((email: string) => {
              return email == userEntity.email ? userEntity : null;
            }),
            create: jest.fn().mockResolvedValue(userEntity),
            update: jest.fn().mockResolvedValue(userEntity),
          },
        },
        {
          provide: getRepositoryToken(RefreshTokenEntity),
          useValue: {
            save: jest.fn().mockResolvedValue(refreshTokenEntity),
            findOne: jest.fn().mockResolvedValue(refreshTokenEntity),
            delete: jest.fn(),
          },
        },
        {
          provide: JwtTokensService,
          useValue: {
            validateRefreshToken: jest.fn().mockReturnValue(payloadDto),
            generateTokens: jest.fn().mockReturnValue(responsePayloadDto),
            saveRefreshToken: jest.fn().mockResolvedValue(refreshTokenEntity),
            findByToken: jest.fn().mockResolvedValue(refreshTokenEntity),
            removeRefreshToken: jest.fn(),
          },
        },
      ],
    }).compile();

    authService = module.get<AuthService>(AuthService);
    usersService = module.get<UsersService>(UsersService);
    jwtTokensService = module.get<JwtTokensService>(JwtTokensService);
    refreshTokenRepository = module.get<Repository<RefreshTokenEntity>>(
      getRepositoryToken(RefreshTokenEntity),
    );
  });

  it('should be defined', () => {
    expect(authService).toBeDefined();
  });

  describe('signUp()', () => {
    it('should succesfully sign up user ', async () => {
      const data: SignUpDto = {
        email: 'user1@test.com',
        password: 'new50vQ72rcoCJm',
        username: 'User',
      };

      const result = await authService.signUp(data);
      expect(result).toEqual(authorizedUserDto);
    });

    it('should exception sign up user', async () => {
      const data: SignUpDto = {
        email: 'user@test.com',
        password: 'new50vQ72rcoCJm',
        username: 'User',
      };

      await expect(authService.signUp(data)).rejects.toBeInstanceOf(
        BadRequestException,
      );
    });
  });

  describe('signIn()', () => {
    it('should succesfully sign in user ', async () => {
      const data: SignInDto = {
        email: 'user@test.com',
        password: '57vQ72rcoCJm',
      };

      const result = await authService.signIn(data);
      expect(result).toEqual(authorizedUserDto);
    });

    it('should exception sign in user', async () => {
      const data: SignInDto = {
        email: 'user@test.com',
        password: 'new50vQ72rcoCJm',
      };

      await expect(authService.signIn(data)).rejects.toBeInstanceOf(
        UnauthorizedException,
      );
    });
  });

  describe('signOut()', () => {
    it('should sign out user', async () => {
      const data: SignOutDto = {
        refresh_token: 'refresh.token',
      };

      const result = await authService.signOut(data);
      expect(result).toBeUndefined();
    });
  });

  describe('refresh()', () => {
    it('should refresh tokens', async () => {
      const data: RefreshDto = {
        refresh_token: 'refresh.token',
      };

      const result = await authService.refresh(data);
      expect(result).toEqual(responsePayloadDto);
    });
  });

  describe('changePassword()', () => {
    it('should change password user', async () => {
      const data: ChangePasswordDto = {
        password: 'newPassword57vQ72rcoCJm',
      };

      const result = await authService.changePassword(payloadDto, data);
      expect(result).toBeUndefined();
    });
  });
});
