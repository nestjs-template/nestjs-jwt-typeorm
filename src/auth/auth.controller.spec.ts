import { Test, TestingModule } from '@nestjs/testing';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import {
  ChangePasswordDto,
  RefreshDto,
  SignInDto,
  SignOutDto,
  SignUpDto,
} from './dto';
import { JwtPayload } from './intrefaces';

const signInDto: SignInDto = {
  email: 'user@test.com',
  password: '57vQ72rcoCJm',
};

const signUpDto: SignUpDto = {
  email: 'user@test.com',
  password: '57vQ72rcoCJm',
  username: 'User',
};

const signOutDto: SignOutDto = {
  refresh_token: 'token',
};

const refreshDto: RefreshDto = {
  refresh_token: 'token',
};

const changePasswordDto: ChangePasswordDto = {
  password: '57vQ72rcoCJm',
};

const payloadDto: JwtPayload = {
  id: '8468b290-fcef-4e11-bf22-b2f99fdc9711',
  email: 'user@test.com',
  username: 'User',
};

describe('AuthController', () => {
  let authController: AuthController;
  let authService: AuthService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [
        AuthService,
        {
          provide: AuthService,
          useValue: {
            signIn: jest
              .fn()
              .mockImplementation(async (data: SignInDto) => data),
            signUp: jest
              .fn()
              .mockImplementation(async (data: SignUpDto) => data),
            signOut: jest.fn(),
            refresh: jest
              .fn()
              .mockImplementation(async (data: RefreshDto) => data),
            changePassword: jest.fn(),
          },
        },
      ],
    }).compile();

    authController = module.get<AuthController>(AuthController);
    authService = module.get<AuthService>(AuthService);
  });

  it('should be defined', () => {
    expect(authController).toBeDefined();
  });

  describe('signIn()', () => {
    it('should call signIn', () => {
      authController.signIn(signInDto);
      expect(authService.signIn).toHaveBeenCalledWith(signInDto);
    });
  });

  describe('signUp()', () => {
    it('should call signUp', () => {
      authController.signUp(signUpDto);
      expect(authService.signUp).toHaveBeenCalledWith(signUpDto);
    });
  });

  describe('signOut()', () => {
    it('should call signOut', () => {
      authController.signOut(signOutDto);
      expect(authService.signOut).toHaveBeenCalledWith(signOutDto);
    });
  });

  describe('refresh()', () => {
    it('should call refresh()', () => {
      authController.refresh(refreshDto);
      expect(authService.refresh).toHaveBeenCalledWith(refreshDto);
    });
  });

  describe('changePassword()', () => {
    it('should call changePassword()', () => {
      authController.changePassword(payloadDto, changePasswordDto);
      expect(authService.changePassword).toHaveBeenCalledWith(
        payloadDto,
        changePasswordDto,
      );
    });
  });
});
