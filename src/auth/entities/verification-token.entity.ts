import { Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';

@Entity('VerificationToken')
@Index(['email', 'token'], { unique: true })
export class VerificationTokenEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  email: string;

  @Column({ unique: true })
  token: string;

  @Column({ type: 'timestamp' })
  expires_at: Date;
}
