import { Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';

@Entity('PasswordResetToken')
@Index(['email', 'token'], { unique: true })
export class PasswordResetTokenEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  email: string;

  @Column({ unique: true })
  token: string;

  @Column({ type: 'timestamp' })
  expires_at: Date;
}
