import {
  Column,
  CreateDateColumn,
  Entity,
  Index, JoinColumn, ManyToOne,
  PrimaryGeneratedColumn,
} from 'typeorm';
import {UserEntity} from "../../users/entities/user.entity";

@Entity('RefreshToken')
@Index(['user_id', 'token'])
export class RefreshTokenEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @CreateDateColumn()
  created_at: Date;

  @Column({ type: 'uuid' })
  user_id: string;

  @Column({ unique: true })
  token: string;

  @ManyToOne(() => UserEntity, {
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'user_id', referencedColumnName: 'id' })
  user: UserEntity;
}
