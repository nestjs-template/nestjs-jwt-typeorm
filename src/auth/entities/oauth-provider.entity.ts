import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  Index,
  ManyToOne,
  JoinColumn,
} from 'typeorm';

import { OAuthProviderName } from '../intrefaces';
import { UserEntity } from '../../users/entities/user.entity';

@Entity('OAuthProvider')
@Index(['user_id'])
@Index(['provider', 'provider_account_id'], { unique: true })
export class OAuthProviderEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn({ nullable: true })
  updated_at: Date;

  @Column({ type: 'uuid' })
  user_id: string;

  @Column({
    type: 'enum',
    enum: OAuthProviderName,
  })
  provider: OAuthProviderName;

  @Column()
  provider_account_id: string;

  @Column({ nullable: true })
  refresh_token: string;

  @Column({ nullable: true })
  access_token: string;

  @Column({ nullable: true })
  expires_at: number;

  @Column({ nullable: true, type: 'json' })
  scope: string;

  @Column({ nullable: true })
  id_token: string;

  @Column({ nullable: true })
  session_state: string;

  @ManyToOne(() => UserEntity, {
    onDelete: 'CASCADE',
  })
  @JoinColumn({ name: 'user_id', referencedColumnName: 'id' })
  user: UserEntity;
}
