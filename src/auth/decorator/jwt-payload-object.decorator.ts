import { createParamDecorator, ExecutionContext } from '@nestjs/common';
import { JwtPayload } from '../intrefaces';

export const JwtPayloadObject = createParamDecorator(
  (_data: unknown, ctx: ExecutionContext): JwtPayload => {
    const request = ctx.switchToHttp().getRequest();
    return request.user;
  },
);
