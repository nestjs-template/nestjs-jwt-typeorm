import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UsersModule } from '../users/users.module';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { UserEntity } from '../users/entities/user.entity';
import { OAuthProviderEntity } from './entities/oauth-provider.entity';
import { VerificationTokenEntity } from './entities/verification-token.entity';
import { RefreshTokenEntity } from './entities/refresh-token.entity';
import { UsersService } from '../users/users.service';
import { jwt_config } from './config/jwt.config';
import { JwtStrategy } from './strategy/jwt.strategy';
import { PasswordResetTokenService } from './password-reset-token.service';
import { VerificationTokenService } from './verification-token.service';
import { JwtTokensService } from './jwt-tokens.service';
import { OAuthProviderService } from './oauth-provider.service';

@Module({
  imports: [
    UsersModule,
    PassportModule,
    JwtModule.register({
      secret: jwt_config.secret,
      signOptions: {
        expiresIn: jwt_config.expiresIn,
      },
    }),
    TypeOrmModule.forFeature([
      UserEntity,
      VerificationTokenEntity,
      OAuthProviderEntity,
      RefreshTokenEntity,
    ]),
  ],
  providers: [
    AuthService,
    UsersService,
    JwtTokensService,
    OAuthProviderService,
    PasswordResetTokenService,
    VerificationTokenService,
    JwtStrategy,
  ],
  controllers: [AuthController],
})
export class AuthModule {}
