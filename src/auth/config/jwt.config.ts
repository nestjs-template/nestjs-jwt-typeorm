export const jwt_config = {
  expiresIn: '15m',
  secret: 'secret-abcdefghij',
  refresh_expiresIn: '3d',
  refresh_secret: 'refresh-abcdefghij',
};
