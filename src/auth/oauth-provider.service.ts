import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { OAuthProviderEntity } from './entities/oauth-provider.entity';

@Injectable()
export class OAuthProviderService {
  public constructor(
    @InjectRepository(OAuthProviderEntity)
    public oauthProviderEntityRepository: Repository<OAuthProviderEntity>,
  ) {}
}
