[circleci-url]: https://circleci.com/gh/nestjs/nest

## Description

[Nest](https://github.com/nestjs/nest) framework TypeScript starter repository.

## Installation

```bash
$ npm install
```

## Copy env file

```bash
$ cp .env.example .env
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

## Migration db

```bash
# migration run
$ npm run migration:run

# migration generate
$ npm run migration:generate --name=your_migration_name

# migration create empty 
$ npm run migration:create --name=your_migration_name

# migration revert
$ npm run migration:revert

```