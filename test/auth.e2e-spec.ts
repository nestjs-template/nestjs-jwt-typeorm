import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { getRepositoryToken, TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from '../src/auth/auth.module';
import { UsersModule } from '../src/users/users.module';
import { ConfigModule } from '@nestjs/config';
import {
  AuthorizedUserDto,
  ChangePasswordDto,
  RefreshDto,
  SignInDto,
  SignOutDto,
  SignUpDto,
} from '../src/auth/dto';
import { In, Repository } from 'typeorm';
import { UserEntity } from '../src/users/entities/user.entity';

const signUpDtoInit: SignUpDto = {
  email: 'user@test.com',
  password: '57vQ72rcoCJm',
  username: 'User',
};

const signInDto: SignInDto = {
  email: 'user@test.com',
  password: '57vQ72rcoCJm',
};

const signUpDto: SignUpDto = {
  email: 'user2@test.com',
  password: '57vQ72rcoCJm',
  username: 'User',
};

const changePasswordDto: ChangePasswordDto = {
  password: 'newPass57vQ72rcoCJm',
};

describe('AuthController (e2e)', () => {
  let app: INestApplication;
  let module: TestingModule;

  let authorizedUserDto: AuthorizedUserDto;
  let signOutDto: SignOutDto;
  let refreshDto: RefreshDto;

  beforeAll(async () => {
    module = await Test.createTestingModule({
      imports: [
        ConfigModule.forRoot({
          envFilePath: ['.env'],
        }),
        TypeOrmModule.forRoot({
          type: 'postgres',
          host: process.env.POSTGRES_HOST,
          port: Number(process.env.POSTGRES_PORT),
          username: process.env.POSTGRES_USER,
          password: process.env.POSTGRES_PASSWORD,
          database: process.env.POSTGRES_DB,
          entities: ['dist/**/*.entity{.ts,.js}'],
          autoLoadEntities: true,
          synchronize: true,
        }),
        AuthModule,
        UsersModule,
      ],
    }).compile();

    app = module.createNestApplication();
    await app.init();

    const { body } = await request(app.getHttpServer())
      .post('/auth/signUp')
      .send(signUpDtoInit);
    authorizedUserDto = body as AuthorizedUserDto;

    const { refresh_token } = authorizedUserDto;

    refreshDto = {
      refresh_token,
    };

    signOutDto = {
      refresh_token,
    };
  });

  afterAll(async () => {
    const userRepository: Repository<UserEntity> = module.get<
      Repository<UserEntity>
    >(getRepositoryToken(UserEntity));
    await userRepository.delete({
      email: In([signUpDto.email, signUpDtoInit.email]),
    });

    await app.close();
  });

  describe('/auth/signUp (POST)', () => {
    it('should sign up user', () => {
      return request(app.getHttpServer())
        .post('/auth/signUp')
        .send(signUpDto)
        .expect(201);
    });
  });

  describe('/auth/signIn (POST)', () => {
    it('should sign in user', () => {
      return request(app.getHttpServer())
        .post('/auth/signIn')
        .send(signInDto)
        .expect(200);
    });
  });

  describe('/auth/refresh (POST)', () => {
    it('should refresh token', () => {
      return request(app.getHttpServer())
        .patch('/auth/refresh')
        .send(refreshDto)
        .set({ Authorization: `Bearer ${authorizedUserDto.access_token}` })
        .expect(200);
    });
  });

  describe('/auth/change-password (POST)', () => {
    it('should change password', () => {
      return request(app.getHttpServer())
        .patch('/auth/change-password')
        .send(changePasswordDto)
        .set({ Authorization: `Bearer ${authorizedUserDto.access_token}` })
        .expect(200);
    });

    it('should sign with new password', () => {
      return request(app.getHttpServer())
        .post('/auth/signIn')
        .send({ ...signInDto, ...changePasswordDto })
        .set({ Authorization: `Bearer ${authorizedUserDto.access_token}` })
        .expect(200);
    });
  });

  describe('/auth/signOut (POST)', () => {
    it('should sign out ', () => {
      return request(app.getHttpServer())
        .post('/auth/signOut')
        .send(signOutDto)
        .set({ Authorization: `Bearer ${authorizedUserDto.access_token}` })
        .expect(200);
    });
  });
});
