import { AuthType } from '@lib/swagger/interfaces/provider.interfaces';

export const GithubSchemaSwagger = {
  properties: {
    token: {
      type: 'string',
      description: 'Github token',
    },
    type: {
      type: 'enum',
      description: 'Device type',
      enum: [AuthType.web],
    },
  },
};
