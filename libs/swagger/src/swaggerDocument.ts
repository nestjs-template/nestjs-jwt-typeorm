export const swaggerDocument = {
  name: 'aipb_backend',
  version: '0.0.1',
  description: '',
  servers: [
    {
      url: '/api',
      description: 'Server',
    },
  ],
  tags: [
    {
      name: 'Users',
      description: `Users`,
    },
    {
      name: 'Auth',
      description: `Authorization and authentication`,
    },
  ],
};
