import { AuthType } from '@lib/swagger/interfaces/provider.interfaces';

export const GoogleSchemaSwagger = {
  properties: {
    token: {
      type: 'string',
      description: 'Google token',
    },
    type: {
      type: 'enum',
      description: 'Device type',
      enum: [AuthType.web],
    },
  },
};
