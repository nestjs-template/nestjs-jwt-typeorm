import { swaggerDocument } from './swaggerDocument';
import { DocumentBuilder } from '@nestjs/swagger';

const { name, version, description, tags, servers } = swaggerDocument;

const config = new DocumentBuilder()
  .setTitle(name)
  .setDescription(description)
  .setVersion(version);

/** Add servers **/
servers.forEach(({ url, description }) => {
  config.addServer(url, description);
});

/** Add tags **/
tags.forEach(({ name, description }) => {
  config.addTag(name, description);
});

export const configDocumentBuilder = config;
